'use strict';

const model = require('./user.model'),
	logger = require('../config/logger').dbLogger;

const userNotFound = new Error('User not found');

exports.save = (user, callback) => {
	var modelData = new model(user);

	logger.debug(`Saving data ${modelData}`);
	modelData.save((error, result) => {
		if(error) {
			logger.error(`Error creating data: ${error}`);
			callback(error);
		} else {
			callback(null, result);
		}
	});
}

exports.update = (id, user, callback) => {
	model.findById(id, (error, result) => {
		if(error) {
			logger.error(`Error finding data for update: ${error}`);
			callback(error);
		} else if(!result) {
			logger.error('Data to update not found');
			callback(userNotFound);
		} else {
			var modelData = new model(user);

			logger.debug(`Updating data ${modelData}`);
			modelData.save((error, result) => {
				if(error) {
					logger.error(`Error updating data: ${error}`);
					callback(error);
				} else {
					callback(null, result);
				}
			});
		}				
	});
}

exports.select = (query, callback) => {		
	if(query && query.id) {
		logger.debug(`Searching data by id ${query.id}`);
		model.findById(query.id, (error, result) => {
			if(error) {
				logger.error(`Error searching data: ${error}`);
				callback(error);
			} else {
				callback(null, result);
			}
		});
	} else {
		logger.debug(`Searching data by ${query}`);
		model.find(query, (error, result) => {
			if(error) {
				logger.error(`Error searching data: ${error}`);
				callback(error);
			} else {
				callback(null, result);
			}
		});
	}
}

exports.delete = (id, callback) => {
	model.findByIdAndRemove(id, (error) => {
		if(error) {
			logger.error(`Error deleting data: ${error}`);
			callback(error);
		} else {
			callback(null, '');
		}
	});
}