'use strict';

require('../env/env.test');

const fs = require('fs'),
	_ = require('lodash');

const processDir = process.cwd();

function readDirectory(path) {
	console.log(`Reading ${path} directory:`);
	let files = fs.readdirSync(path);

	console.log(`Files found: ${files}`);
	_.forIn(files, (file, key) => {
		if (fs.statSync(`${path}/${file}`).isDirectory()) {
      readDirectory(`${path}/${file}`);
    } else if(file.endsWith('spec.js')){
    	console.log(`Loading ${path}/${file} test file`);
      require(`${path}/${file}`);
    } else {
    	console.log(`File ${path}/${file} is not a test file`);
    }
	});	
}

readDirectory(`${processDir}/api`);