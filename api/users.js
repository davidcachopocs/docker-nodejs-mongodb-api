'use strict';

const repository = require('./users.repository'),
	logger = require('../config/logger').appLogger;

exports.getUsers = getUsersFn;
exports.addUser = addUserFn;
exports.updateUser = updateUserFn;
exports.deleteUser = deleteUsersFn;

function getUsersFn(request, response, next) {
	let query = {};
	if(request.params.id) {
		query.id = request.params.id;
	} else {
		query = request.query;
	}

	logger.info(`Get users by ${JSON.stringify(query)}`);
	repository.select(query, (error, result) => {
		if(error) {
			response.status(400).send(error);
		} else {
			response.status(200).send(result);
		}
	});
}

function addUserFn(request, response, next) {
	logger.info(`Add user ${JSON.stringify(request.body)}`);
	repository.save(request.body, (error, result) => {
		if(error) {
			response.status(400).send(error);
		} else {
			response.sendStatus(201);
		}
	});
}

function updateUserFn(request, response, next) {
	if(!request.params.id) {
		response.status(400).send('Identifier not defined');
	}

	logger.info(`Update user ${request.params.id}: ${JSON.stringify(request.body)}`);
	repository.update(request.params.id, request.body, (error, result) => {
		if(error) {
			response.status(400).send(error);
		} else {
			response.sendStatus(204);
		}
	});
}

function deleteUsersFn(request, response, next) {
	if(!request.params.id) {
		response.status(400).send('Identifier not defined');
	}

	logger.info(`Delete user ${request.params.id}`);
	repository.delete(request.params.id, (error) => {
		if(error) {
			response.status(400).send(error);
		} else {
			response.sendStatus(204);
		}
	});
}