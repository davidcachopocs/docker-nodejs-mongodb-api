'use strict';

const usersApi = require('../api/users');

module.exports = (router) => {
	router.route('/api/users/:id?')
		.get((request, response, next) => {
			usersApi.getUsers(request, response, next);
		})
		.post((request, response, next) => {
			usersApi.addUser(request, response, next);
		})
		.put((request, response, next) => {
			usersApi.updateUser(request, response, next);
		})
		.delete((request, response, next) => {
			usersApi.deleteUser(request, response, next);
		});
};