'use strict';

const logger = require('../config/logger').routerLogger;

module.exports = (router) => {
	router.use((request, response, next) => {
		logger.info(`${request.method} request to ${request.url} [query: ${JSON.stringify(request.query)}] [body: ${JSON.stringify(request.body)}]`);
		next();
	});

	router.use((request, response, next) => {
		response.header('Access-Control-Allow-Credentials', true);
	  response.header("Access-Control-Allow-Origin", request.headers.origin);
	  response.header("Access-Control-Allow-Methods", "OPTIONS, GET, POST, DELETE, PUT");   
	  response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Cookie");
		next();
	});
};