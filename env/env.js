'use strict';

const _ = require('lodash');

const defaultProperties = {
	LOGGER_DIRECTORY: './logs',
	EXCEPTIONS_FILENAME: 'exceptions.log',
	LOGGER_FILENAME: 'app.log',
	LOGGER_LEVEL: 'debug',
	ROUTER_LOGGER_FILENAME: 'router.log',
	ROUTER_LOGGER_LEVEL: 'debug',
	DB_LOGGER_FILENAME: 'db.log',
	DB_LOGGER_LEVEL: 'debug',
	PORT: 9000,
	MONGO_URI: 'mongodb://localhost:27017'
}

_.forIn(defaultProperties, (value, key) => {
	process.env[key] = process.env[key] || value;
});

const logger = require('../config/logger').appLogger;

_.forIn(defaultProperties, (value, key) => {
	logger.warn(`Environment property: ${key}:${process.env[key]}`);
});