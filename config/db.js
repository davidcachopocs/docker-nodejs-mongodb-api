'use strict';

const mongoose = require('mongoose'),
		events = require('./events'),
		logger = require('./logger').dbLogger;			
		//seed = require('./seed'),
		//model = require('../api/customers/customers.model');

let dataBase = null;

exports.dataBase = dataBase;
/*
events.on('loadSeed', function() {
	logger.info('Loading seed data to customers...');
	model.count({}, function(error, result) {
		if(error) {
			logger.error('Error loading seed data: ' + error);
		} else if(result > 0) {
			logger.warn('customers already has data. Seed not loaded');
		} else {
			var modelData = new model(seed);
			modelData.save(function(error, result) {
				if(error) {
					logger.error('Error loading seed data: ' + error);
				} else {
					logger.info('Seed data loaded');
				}
			});
		}
	});	
});
*/
function connect() {
	logger.info(`Connecting to database ${process.env.MONGO_URI}`);
	mongoose.connect(process.env.MONGO_URI);
	dataBase = mongoose.connection;

	dataBase.on('error', (error) => {
		logger.error(`Database connection error: ${error}`);
		process.exit(-1);
	});
	
	dataBase.on('open', () => {
		logger.info('Successfully connected to database');
		events.emit('dbSuccessConnection');

		/*if(!process.env.NODE_ENV || process.env.NODE_ENV === 'dev') {
			events.emit('loadSeed');	
		}*/
	});	
	
	dataBase.on('close', () => {
		logger.warn('Database connection closed');
	});			
}

connect();