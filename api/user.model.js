'use strict';

const mongoose = require('mongoose'),
	Schema	 = mongoose.Schema,
	ObjectId = Schema.ObjectId,
	logger = require('../config/logger').dbLogger;

const schema = new Schema({
	id: {
		type: ObjectId
	},
	username: {
		type: String,
		required: [true, 'Username required'],			
		maxlength: 25,
		minlength: 3,
	},
	email: {
		type: String,
		validate: {
			validator: function(value) {
				return /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value);
			},
			message: 'Invalid email address'
		}
	},
	age: {
		type: Number,
		max: [100, 'Not allowed over 100'],
		min: [18, 'Not allowed under 18']
	},
	created: Date,
	createdBy: {
		type: String,
		required: [true, 'Created by undefined']
	},
	updated: Date,
	updatedBy: String
}, {collation: {locale: 'es', strength: 1}});

const model = mongoose.model('user', schema);

schema.pre('save', function(next) {
	if(!this.created) {
		logger.debug(`Pre save on creating new data`);
		this.created = new Date();
	} else {
		logger.debug(`Pre save on updating data ${this.id}`);
		this.updated = new Date();
	}

	next();
});

module.exports = model;